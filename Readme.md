# Django API Creation

Project were we create and consume APIs with Django

This project is based in:
- Python 3.10.9
- Django
- djangorestframework: https://www.django-rest-framework.org/
- Postman

Libraries and others:
- Django==3.2.5
- djangorestframework==3.14.0

Steps procedure:
- Define quantity of apps
- Creation and implementation of models
- Creation of serializers
- Design and implement Django REST framework
- Validate functionalities using Postman